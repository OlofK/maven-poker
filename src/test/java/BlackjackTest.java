package blackjack;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BlackjackTest {
	
	Blackjack bj = Blackjack.getInstance();
	List<Card> hand;
	
//	Ny hand före varje testmetod
	@BeforeEach
	void resetHand() {
		hand = new ArrayList<>();
	}
	
//	Test av score()-metoden och att ett ess kan räknas med värdet 11. Summan ska bli 19 (8 + 11)
	@Test
	void testScoreAceEleven() {
		hand.add(new Card(Suit.HEARTS, "Eight"));
		hand.add(new Card(Suit.CLUBS, "Ace"));
		assertEquals(19, bj.score(hand));
	}
	
//	Test av score()-metoden och att ett ess kan räknas med värdet 1. Summan ska bli 19 (8 + 1 + 10)
	@Test
	void testScoreAceOne() {
		hand.add(new Card(Suit.CLUBS, "Eight"));
		hand.add(new Card(Suit.DIAMONDS, "Ace"));
		hand.add(new Card(Suit.HEARTS, "King"));
		assertEquals(19, bj.score(hand));
	}
}

