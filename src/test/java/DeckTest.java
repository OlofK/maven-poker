package blackjack;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class DeckTest {
	
	Deck deck;
	List<Card> hand;
	
//	Ny Deck före varje testmetod
	@BeforeEach
	void newDeck() {
		deck = new Deck();
	}
	
//	Hjälpmetod som blandar kortleken och returnerar antalet kort (används av testmetoder)
	int shuffleAndGetNumberOfCards() {
		deck.shuffle();
		int numberOfCards = deck.getDeck().size();
		return numberOfCards;
	}
	
//	Testmetod som testar att kortleken innehåller 52 kort efter blandning m.h.a. JUnits assertEquals()
	@Test
	void testShuffle() {
		assertEquals(52, shuffleAndGetNumberOfCards());
	}
	
//	Parameteriserat test av ovanstående som provar att dra 1, 2 respektive 3 kort
//	samt blandar kortleken
	@ParameterizedTest
	@ValueSource(ints = {1,2,3})
	void testShuffleWithDraw(int n) {
		for (int i = 1; i <= n; i++) {
			deck.draw();
		}
		assertEquals(52 - n, shuffleAndGetNumberOfCards());
	}
		
//	Test som kontrollerar att det finns ett kort på handen (med assertNotNull()) efter anropande av draw()-metoden
	@Test
	void cardExists() {
		hand = new ArrayList<>();
		hand.add(deck.draw());
		assertNotNull(hand);
	}
	
//	Test för att se om det finns dubbletter i kortleken. Blir true om alla kort är olika (med assertFalse()).
	@Test
	void testDoubles() {
		for (int i = 0; i < deck.getDeck().size(); i++) {
			for (int j = 0; j < deck.getDeck().size(); j++) {
				if (i != j) {
					assertFalse(deck.getDeck().get(i).equals(deck.getDeck().get(j)));
				}
			}
		}
	}
	
//	Samma som ovanstående, men med blandning av kortleken. Körs 5 gånger genom annotationen 
//	@RepeatedTest(5) för att se att resultatet blir samma varje gång.
	@RepeatedTest(5) 
	void testDoublesWithShuffle() {
		deck.shuffle();
		for (int i = 0; i < deck.getDeck().size(); i++) {
			for (int j = 0; j < deck.getDeck().size(); j++) {
				if (i != j) {
					assertFalse(deck.getDeck().get(i).equals(deck.getDeck().get(j)));
				}
			}
		}
	}
}
